var db = require('../mongo/connector');
var crypto = require('crypto');

var collection = 'accounts';

var controller = {
	create: function(account, callback) {
		if(!(account.password && account.user))
			return callback({ code: 400, message: "Bad Request"});


		crypto.randomBytes(256, function(err, buf){
			if (err) {
				if (typeof callback === "function")
					callback({ code: 500, message: "Internal Error"});
				return console.log(err);
			}
			
			account.salt = buf.toString('hex');
			crypto.pbkdf2(account.password, account.salt, 100000, 512, 'sha512', function(err, key) {
				if (err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error"});
					return console.log(err);
				}
				
				account.password = key.toString('hex');
				account.status = "Complete";
				db.save(collection, account, callback);
			});
		});
	}
};

var self = module.exports = controller;
