var db = require('../mongo/connector');
var crypto = require('crypto');

var tokensCollection = 'tokens';
var accountsCollection = 'accounts';

var controller = {
	login: function(account, callback) {
		db.find(accountsCollection, { user: account.user }, function (err, docs) {
			if (err) {
				if (typeof callback === "function")
					callback({ code: 404, message: "User or Password incorrect"});
				return console.log(err);
			}

			if(docs.length == 1) {
				crypto.pbkdf2(account.password, docs[0].salt, 100000, 512, 'sha512', function(err, key) {
					if (err) {
						if (typeof callback === "function")
							callback({ code: 500, message: "Internal Error"});
						return console.log(err);
					}
					
					if(key.toString('hex') === docs[0].password) {
						var d = new Date();
						var token = {
							date: d,
							logged_in: true,
							usedby: new Date(d.getTime() + 4*86400000),
							token: crypto.randomBytes(256).toString('hex'),
							account_id: docs[0]._id
						}
						var user_token = {
							date: token.date,
							usedby: token.usedby,
							token: token.token
						}
						db.save(tokensCollection, token, function(err){
							if (err) {
								if (typeof callback === "function")
									callback({ code: 500, message: "Internal Error"});
								return console.log(err);
							}
							callback(null, user_token);
						});
					} else {
						if (typeof callback === "function")
							callback({ code: 404, message: "User or Password incorrect"});
					}
				});
			} else {
				callback({ code: 404, message: "User or Password incorrect"});
			}
		});
	},
	validateToken: function(reqToken, callback) {
		return db.find(tokensCollection, { token: reqToken }, function(err, docs){
			if (err) {
				if (typeof callback === "function")
					callback({ code: 500, message: "Internal Error"});
				return console.log(err);
			}

			if(docs.length == 1 && docs[0].usedby.getTime() > new Date().getTime() && docs[0].logged_in) {
				if(typeof callback === "function")
					callback(null, docs[0].account_id);
			} else {
				callback({ code: 404, message: "Invalid Token"});
			}
		});
	},
	logout: function (reqToken, callback) {
		return db.updateBy(tokensCollection, { token: reqToken }, { $set:{logged_in: false} }, function(err, docs){
			if (err) {
				if (typeof callback === "function")
					callback({ code: 500, message: "Internal Error"});
				return console.log(err);
			}
			if(docs.result.ok == 1 && docs.result.nModified == 1) {
				if(typeof callback === "function")
					callback(null, { response: "User logged out!"} );
			} else {
				callback({ code: 404, message: "Invalid Token"});
			}
		});
	}
};

var self = module.exports = controller;
