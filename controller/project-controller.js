var fs = require('fs');
var multer  = require('multer');
var randomstring = require("randomstring");

var db = require('../mongo/connector');

var loginCtr = require('./login-controller');

var collection = 'projects';
var ObjectID = require('mongodb').ObjectID;

var controller = {
	create: function(project, token, callback) {
		loginCtr.validateToken(token, function(err, account_id) {
			if (err) { return callback(err); }
			
			project.account_id = account_id;
			db.save(collection, project, function(err, result){
				if(err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error"});
					return console.log(err);
				}

				callback(null, project);
			});
		});
	},
	update: function(project, token, callback) {
		loginCtr.validateToken(token, function(err, account_id) {
			if (err) { return callback(err); }
			
			project.account_id = account_id;
			db.update(collection, project, function(err, result){
				if(err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error"});
					return console.log(err);
				}

				callback(null, project);
			});
		});
	},
	get: function(id, callback){
		criteria = {
			_id: new ObjectID(id)
		};
		db.find(collection, criteria, function(err, result) {
			if(err) {
				if (typeof callback === "function")
					callback({ code: 500, message: "Internal Error"});
				return console.log(err);
			}

			callback(null, result);
		});
	},
	getAll: function(callback){
		db.find(collection, {}, function(err, result) {
			if(err) {
				if (typeof callback === "function")
					callback({ code: 500, message: "Internal Error"});
				return console.log(err);
			}

			callback(null, result);
		});
	},
	upload_image: function(req, res, callback) {
		var token = req.get("Authorization");
		var id = req.params.id;
		var dir = 'public/' + id;
		console.log("Controller - Project - upload_image");

		loginCtr.validateToken(token, function(err, account_id) {
			if (err) { return callback(err); }
			
			console.log("Login - OK");
			
			if (!fs.existsSync(dir)){
				fs.mkdirSync(dir);
			}
			
			var name = randomstring.generate(7);

			var storage = multer.diskStorage({
				destination: function (req, file, cb) {
					cb(null, dir);
				},
				filename: function (req, file, cb) {
					var size = file.originalname.length;
					var dot = file.originalname.lastIndexOf('.');
					name += file.originalname.substring(dot,size);
					console.log(name);
					cb(null, name);
				}
			});

			var upload = multer({ storage: storage }).single("userfile");
			
			upload(req, res, function(err) {
				if(err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error"});
					return console.log(err);
				}
	

				console.log('body: ', req.body);

				var criteria = { _id: new ObjectID(id) };
				db.find(collection, criteria, function(err, result) {
					if(err) {
						if (typeof callback === "function")
							callback({ code: 500, message: "Internal Error"});
						return console.log(err);
					}

					newPhoto = {};
					newPhoto.title = "New Photo";
					newPhoto.description = "New Photo Description";
					newPhoto.width = 0;
					newPhoto.height = 0;
					newPhoto.visible = true;
					newPhoto.src = id + "/" + name;
					
					result[0].photos.push(newPhoto);

					db.update(collection, result[0], function(err, result){
						if(err) {
							if (typeof callback === "function")
								callback({ code: 500, message: "Internal Error"});
							return console.log(err);
						}
						callback(null, newPhoto);
					});
				});
			});
		});
	},
	removePhoto: function(id, src, callback) {
		var criteria = { _id: new ObjectID(id) };
		db.find(collection, criteria, function(err, result) {
			if(err) {
				if (typeof callback === "function")
					callback({ code: 500, message: "Internal Error"});
				return console.log(err);
			}

			for(var i=0; i<result[0].photos.length; i++) {
				if(result[0].photos[i].src === src) {
					result[0].photos.splice(i, 1);
					break;
				}
			}
								
			db.update(collection, result[0], function(err, result){
				if(err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error"});
					return console.log(err);
				}
				callback(null, { code:200, mesage: "Photo removed" });
			});
		});
	},
	delete_image: function(req, res, callback) {
		var token = req.get("Authorization");
		var id = req.params.id;
		var src = req.body.src;
		var dir = 'public/' + src;

		console.log("Controller - Project - delete_image");

		loginCtr.validateToken(token, function(err, account_id) {
			if (err) { return callback(err); }
			
			console.log("Login - OK");
		
			if(!src.startsWith("http")) {
				fs.access(dir, fs.constants.F_OK, (err) => {
					console.log(`${dir} ${err ? 'does not exist' : 'exists'}`);
					if(err) {
						console.log(err);
					} else {
						fs.unlink(dir, (err) => {
							if(err) {
								console.log(err);
							}
						});
					}
				});
			}

			controller.removePhoto(id, src, callback);
		});
	}
};

var self = module.exports = controller;
