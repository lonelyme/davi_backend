var express = require('express');
var bparser = require('body-parser');
var routes = require('./route/create-routes');

var cors = require('cors');

var app = express();

var port = 3184;

app.use(cors());
app.use(express.static('public'));
app.use(bparser.urlencoded({ extended: true }));
app.use(bparser.json({verify:function(req,res,buf){req.rawBody=buf}}));
//app.use(printRequest);

routes.insert(app);

app.use(logErrors);
app.use(bodyParseErrorHandler);
app.use(errorHandler);


app.listen(port, function () {
  console.log('Davi Server is now listening on port %d!', port);
})

process.on('SIGINT', () => {
	console.log('Received SIGINT. Bye!');
	process.exit();
});

process.on('SIGTERM', () => {
	console.log('Received SIGTERM. Bye!');
	process.exit();
});

function printRequest(err, req, res, next) {
	console.log("New request: ", req.method, " ", req.url);
	console.log("From: ", req.headers.host);
	console.log("Body: %s", req.rawBody);
}

function logErrors (err, req, res, next) {
	console.error("Error log: ", err.stack)
	next(err, req)
}

function bodyParseErrorHandler (err, req, res, next) {
	if (err instanceof SyntaxError) {
		console.log("Body Parse Error");
		res.status(400).send({ error: 'Invalid JSON Format' });
	} else {
		next(err, req);
	}
}

function errorHandler (err, req, res, next) {
	console.log("Final Error Handler Function");
	res.status(500).send({ error: 'Something failed!' });
}

