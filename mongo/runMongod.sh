#!/bin/bash

pushd `dirname $0` > /dev/null
SCRIPT_PATH=`pwd -P`
popd > /dev/null

export DB_PATH="$SCRIPT_PATH/data/db"
echo $DB_PATH
export LOG_PATH="$SCRIPT_PATH/mongo.log"
echo $LOG_PATH

export OPTIONS=""

while [ "$1" != "" ]; do
	case $1 in
		-A | --auth )	OPTIONS="$OPTIONS --auth"
						;;
		* )				echo "Invalid Parameter"
						exit 1
	esac
	shift
done

echo $OPTIONS

mkdir -p $DB_PATH

#mongod --auth --port 3284 --dbpath $DB_PATH > $LOG_PATH 2>&1 &
#mongod --port 3284 --dbpath $DB_PATH > $LOG_PATH 2>&1 &
mongod $OPTIONS --port 3284 --dbpath $DB_PATH > $LOG_PATH 2>&1 &
echo $!
