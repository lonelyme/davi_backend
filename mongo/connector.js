var mongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;

var URL='mongodb://pensador_davi:pensador_davi@localhost:3284/portfolio';

var connect = function(callback) {
	mongoClient.connect(URL,function(err, db) {
		if (typeof callback === "function") {
			if(err) { 
				callback(err);
				return console.log(err); 
			}
			callback(null, db);
		}
	});
};

var database = {
	save: function(collection, data, callback) {
		connect(function(err, db) {
			if(err) { console.log("Erro"); return callback({ code: 500, message: "Internal Error" }); }

			db.collection(collection).save(data, function(err, result) {
				if(err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error" });
					return console.log(err); 
				} else {
					if (typeof callback === "function")
						callback(null, result);
				}

				db.close();
			});
		});
	},
	updateBy: function(collection, criteria, data, callback) {
		connect(function(err, db) {

			if(err) { console.log("Erro"); return callback({ code: 500, message: "Internal Error" }); }
			
			db.collection(collection).updateOne(criteria, data, function(err, result) {
				
				if(err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error" });
					return console.log(err); 
				} else {
					if (typeof callback === "function")
						callback(null, result);
				}

				db.close();
			});
		});
	},
	update: function(collection, data, callback) {
		connect(function(err, db) {

			if(err) { console.log("Erro"); return callback({ code: 500, message: "Internal Error" }); }

			data._id = new ObjectID(data._id);
			db.collection(collection).updateOne({ _id : data._id }, data, function(err, result) {
				
				if(err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error" });
					return console.log(err); 
				} else {
					if (typeof callback === "function")
						callback(null, result);
				}

				db.close();
			});
		});
	},
	find: function(collection, data, callback) {
		connect(function(err, db) {
			if(err) { return callback({ code: 500, message: "Internal Error" }); }
			db.collection(collection).find(data).toArray(function(err, result) {
				if(err) {
					if (typeof callback === "function")
						callback({ code: 500, message: "Internal Error" });
					return console.log(err); 
				} else {
					if (typeof callback === "function")
						callback(null, result);
				}

				db.close();
			});
		});
	}
};

var self = module.exports = database;
	
