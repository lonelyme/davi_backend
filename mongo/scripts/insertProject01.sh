#!/bin/bash

key="f883179be3a708bb08458c26f005f6eaef481cf5f23ac2ca2fba7b2820a7eb36fe5219ad78ff3ec413dd722de1d7170fbdb5c35feefc51f679aaa4267541ba96d80a937c6a7b9a6e944ed831738b605274a6d4f348dd864f89a31124d4c9f44db5c614b7ef957f1eca306d797104bfdbc6de79dc0f1a62f60c5dd6e438b61cfedd968562c371f82ba0be493e38e37b3a398d6a709b86c6522af926d775759f5b0a81c564ca8327cfa311c5b486cd83d9b5c8dc4af7e725783d09e382e5cf1c07221b229b55c5be15ada85053dd6e01763b093638a2536e40d570ce4acfc0858afc84ab045cd2fc8ad2ed68925b729e4fdc488879a476c260a2b3b79dfdee6257"

curl -i -X POST -H "Content-Type:application/json" -H "Authorization:$key" http://localhost:3184/projects -d "{
   \"projectName\":\"Test01\",
   \"projectDesc\":\"Th1s 1s ju5t 4 t3st\",
   \"photos\":[
		{
			\"src\":\"https://lifetouch.com/wp-content/uploads/2018/06/careerstrailerbox.jpg\",
			\"title\":\"Photo0\",
			\"description\":\"Photo0\",
			\"width\":620,
			\"height\":635 
		},
		{
			\"src\":\"https://images.unsplash.com/photo-1542838686-b08706f6f2d1\",
			\"title\":\"Photo1\",
			\"description\":\"Photo1\",
			\"width\": 550,
			\"height\": 397
		}
   ],
   \"cover\":0
}"
