use admin;

db.createUser(
	{ 
		user: "root", 
		pwd: "root", 
		roles: [ "userAdminAnyDatabase" ] 
	}
);

use portfolio;

db.createUser(
	{ 
		user: "pensador_davi", 
		pwd: "pensador_davi", 
		roles: [ 
			{ role: "readWrite", db: "portfolio" }
		] 
	}
);

db.createCollection( "accounts",
	{ 
		validator: {
			$and: [
				{ user: { $type: "string" } },
				{ password: { $type: "string" } },
				{ status: { $in: [ "Verification", "Complete" ] } }
			]
		}
	}
);

db.accounts.createIndex( { user: 1 } , { unique: true });

db.createCollection( "projects",
	{ 
		validator: {
			$and: [
				{ projectName: { $type: "string" } },
				{ projectDesc: { $type: "string" } }
			]
		}
	}
);

db.createCollection( "about",
	{ 
		validator: {
			$and: [
				{ date: { $type: "date" } },
				{ message: { $type: "string" } },
				{ account_id: { $type: "objectId" } }
			]
		}
	}
);

db.about.createIndex( { date: -1 } );
