#!/bin/bash

key="92310674142a156a7b6ab640858c0d1bf041e47f09bf648ee83fbd3699cb884f6ca2353b7c14ab3b5f30cdc9eb9e12b83132579cffd6c42a28467a5e8505f3daf5e6e403fc2df8fda581eff8d1057cc28ad36f5eafcee1b133350bd837078d4e161e7aad768fc618633e089fb4c8adefcb9b0518e998eaef89e451b60b306520e4bdc14cb8c7c51c52762b02f2e017dae7e79a9277fc7ea4fffdf7068a9bc28ae4dcfc16e4a61b0b6078110f3173d234860eaae49eae00239d4fba1c2f93a55143a0849fc76d1029f392a24bdf668df1e27d264c6dae0df8599987769758fa1dbebbe792eba2b0c1dacc9cb8b253599d22b6b5095105c440ad7e24ac09f0f502"

curl -i -X POST -H "Content-Type:application/json" -H "Authorization:$key" http://localhost:3184/projects -d "{
	\"projectName\":\"Test02\",
	\"projectDesc\":\"Th1s 1s ju5t 4 t3st\",
	\"photos\":[
		{ 
			\"src\":\"http://4.bp.blogspot.com/-HUlJittwCdQ/Ut-xgoYjyzI/AAAAAAAABNs/cKSIaI7g8JA/s1600/fome+no+mundo.jpg\",
			\"title\":\"Photo0\",
			\"description\":\"Photo0\",
			\"width\":584,
			\"height\":366
		},
		{ 
			\"src\":\"https://media.newyorker.com/photos/59095bce2179605b11ad4d12/4:3/w_620,c_limit/Stillman-Hiroshima.jpg\",
			\"title\":\"Photo1\",
			\"description\":\"Photo1\",
			\"width\":620,
			\"height\":465
		},
		{ 
			\"src\":\"http://d2mhmd1y9dvsiv.cloudfront.net/wp-content/uploads/sites/5/2017/04/05/siria-reacciones-ataque-700x440.jpg\",
			\"title\":\"Photo2\",
			\"description\":\"Photo2\",
			\"width\":700,
			\"height\":440
		},
		{ 
			\"src\":\"https://www.genevaupdates.com/wp-content/uploads/2016/08/photo709857605135216421-720x540-720x540.jpg\",
			\"title\":\"Photo3\",
			\"description\":\"Photo3\",
			\"width\":720,
			\"height\":540
		}
	],
	\"cover\":2
}"

