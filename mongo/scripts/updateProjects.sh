#!/bin/bash

curl -i -X PUT -H "Content-Type:application/json" -H "Authorization:c28d816b4ef31528f801e8234c1a30dfb34d26c400e91fca588f8cf4046b0774a006cfff1e84c4b3b0ce4ae8e09c942a15f4b4d2d7a43943662d1ffeb21a9652f2f8babcab2b3c0d5e1d3d0d50aef813d1233d870aa12b232dab7778fa5f744af0ceea6ccef10ee6696330b0de65623eb2cd3cb369908785be9e4bef09384c4eb3d97e61712ca3ca3d7b592230ba39f787f9a638d97efc0d3169e5378d2b47cd6021381f4a03b5b99b820c305aa19dde4e0c282c44347e7337e4b734b870ce7161cc10ac763c818b9b0315f23fa6dbbb09a9d470fd36de119e7479c8b67c42f04b9710cad51a80c041d9d14e27b81bea5ef25522dae1de99f542be0193c160db" http://localhost:3184/projects -d "{
	\"_id\" : \"5a99469f29b084291b05adfa\",
	\"projectName\" : \"Test00\",
	\"projectDesc\" : \"Th1s 1s ju5t 4 t3st\",
	\"photos\" : [
		\"http://i1.wp.com/womensilab.com/wp-content/uploads/2015/04/sunsetyoga.jpg\",
		\"http://www.danceflowlift.com/wp-content/uploads/2015/10/Beach-Yoga-1.jpg\"
	]
}"

