var express = require('express');
var router = express.Router();

var controller = require('../controller/account-controller');

router.post('/', function(req, res, next) {
	controller.create(req.body, function(err, result){
		if(err) {
			res.status(err.code);
			res.json({ "error" : err.message });
		} else {
			res.json(result);
		}
	});
});

var self = module.exports = router;
