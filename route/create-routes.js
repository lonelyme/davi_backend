var login = require('./login-router');
var logout = require('./logout-router');
var accounts = require('./accounts-router');
var projects = require('./projects-router');

var model = {
	insert: function(app){
		app.use('/login', login);
		app.use('/logout', logout);
		app.use('/accounts', accounts);
		app.use('/projects', projects);

		// catch 404 and forward to error handler
		app.use(function(req, res, next) {
			res.status(404);
			res.json({"error": "Not Found"});
		});
	}
};

var self = module.exports = model;
