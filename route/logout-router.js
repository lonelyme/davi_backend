var express = require('express');
var router = express.Router();

var controller = require('../controller/login-controller');

router.post('/', function(req, res, next) {
	console.log("POST logout");
	controller.logout(req.get("Authorization"), function(err, result){
		if(err) {
			res.status(err.code);
			res.json({ "error" : err.message });
		} else {
			res.json(result);
		}
	});
});

var self = module.exports = router;
