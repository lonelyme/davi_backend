var express = require('express');
var router = express.Router();

var controller = require('../controller/project-controller');

router.post('/', function(req, res, next) {
	console.log("POST project ", req.body, " - Token: ", req.get("Authorization"));
	controller.create(req.body, req.get("Authorization"), function(err, result){
		if(err) {
			res.status(err.code);
			res.json({ "error" : err.message });
		} else {
			res.json(result);
		}
	});
});

router.put('/', function(req, res, next) {
	console.log("PUT project", req.body);
	controller.update(req.body, req.get("Authorization"), function(err, result){
		if(err) {
			res.status(err.code);
			res.json({ "error" : err.message });
		} else {
			res.json(result);
		}
	});
});

router.get('/', function(req, res, next) {
	controller.getAll(function(err, result){
		if(err) {
			res.status(err.code);
			res.json({ "error" : err.message });
		} else {
			res.json(result);
		}
	});
});

router.get('/:id', function(req, res, next) {
	controller.get(req.params.id, function(err, result){
		if(err) {
			res.status(err.code);
			res.json({ "error" : err.message });
		} else {
			if(result.length > 0)
				result = result[0];
			else 
				result = {};

			res.json(result);
		}
	});
});

router.post('/:id/image', function(req, res, next) {
	console.log("POST project: ", req.params.id, " upload-image ", req.body);
	controller.upload_image(req, res, function(err, result){
		if(err) {
			res.status(err.code);
			res.json({ "error" : err.message });
		} else {
			res.json(result);
		}
	});
});

router.delete('/:id/image', function(req, res, next) {
	console.log("DELETE project: ", req.params.id, " delete-image ", req.body);
	controller.delete_image(req, res, function(err, result){
		if(err) {
			res.status(err.code);
			res.json({ "error" : err.message });
		} else {
			res.json(result);
		}
	});
});

var self = module.exports = router;
