#!/bin/bash

LOG_FILE="run-app.log"

nohup node app.js >> $LOG_FILE 2>&1 &
export NODEPID=$!

echo "" >> $LOG_FILE
echo " -- // -- " >> $LOG_FILE
echo "" >> $LOG_FILE
echo "New server instance - PID: $NODEPID" >> $LOG_FILE
echo $NODEPID
